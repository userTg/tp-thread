package org.eclipse.thread.tp1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Dummy {

	public static void main(String[] args) {

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println("Thread runnable " + Thread.currentThread().getName());
			}

		});
		t.start();

		Thread t1 = new Thread(() -> {
			System.out.println("Thread runnable " + Thread.currentThread().getName());
		});

		t1.start();
		
		System.out.println("--------------------------------------------------------");

		List<Machine> listMachines = new ArrayList();
		Machine o1 = new Machine(1910, "berliet");
		Machine o2 = new Machine(1900, "bouton");
		listMachines.add(o1);
		listMachines.add(o2);
		Comparator<Machine> comp = new Comparator<Machine>() {

			@Override
			public int compare(Machine o1, Machine o2) {
				// TODO Auto-generated method stub
				return o1.getAnne() - o2.getAnne();
			}
		};
		
		Collections.sort(listMachines, comp);
		listMachines.forEach(System.out::println);
		
		Comparator comp1 = (O1,O2)->{
			 int i=(((Machine) o1).getAnne() - ((Machine) o2).getAnne());
			 return i;
		};
		
		Collections.sort(listMachines, comp1);
		listMachines.forEach(System.out::println);
//		comp.compare(o1, o2);
//		System.out.println(comp.compare(o1, o2));
		
		
	}

}
