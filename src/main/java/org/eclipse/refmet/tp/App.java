package org.eclipse.refmet.tp;

import java.util.function.Consumer;

public class App {

	public App() {
	}

	public App(String s) {
		System.out.println("Construction à partir de " + s);
	}

	public static void applyString(Consumer<String> c, String str) {
		c.accept(str);
	}

	public static void execute(String commande) {
		System.out.println("Execution de la commande " + commande);
	}

	public void executeObj(String commande) {
		System.out.println("Execution de la commande " + commande);
	}

	public static void main(String[] args) {

		applyString(System.out::println, "Salut");
		// Méthode statique
		applyString(App::execute, "ls");

		App a = new App();

		// méthode d'instance
		applyString(a::executeObj, "cp");

		// utilisation du constructeur
		applyString(App::new, "top");

	}

}
