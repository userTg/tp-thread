package org.eclipse.thread.tp1;

public class Machine {
	
	private int annee;
	private String nom;
	
	
	public Machine() {
		super();
	}


	public Machine(int anne, String nom) {
		super();
		this.annee = anne;
		this.nom = nom;
	}


	public int getAnne() {
		return annee;
	}


	public void setAnne(int anne) {
		this.annee = anne;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	@Override
	public String toString() {
		return "Machine [anne=" + annee + ", nom=" + nom + "]";
	}
	
	

}
