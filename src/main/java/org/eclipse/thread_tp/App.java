package org.eclipse.thread_tp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class App {

	public static class MyThread extends Thread {

		public void run() {
			System.out.println("Thread runnable " + Thread.currentThread().getName());
		}
	}

	public static void main(String[] args) throws Exception {
		// MyThread t = new MyThread();
		// t.start();
		/*
		 * Thread t = new Thread(new Runnable() {
		 * 
		 * @Override public void run() { System.out.println("Thread runnable " +
		 * Thread.currentThread().getName()); }
		 * 
		 * });
		 */
		Thread t = new Thread(() -> {
			System.out.println("Thread runnable " + Thread.currentThread().getName());
		});

		t.start();

		for (int i = 0; i < 10; i++) {
			System.out.println("Hello " + i);
		}
		System.out.println("Thread runnable " + Thread.currentThread().getName());

		
		ExecutorService es = Executors.newFixedThreadPool(5);
		/*
		Future<Integer> ret = es.submit(new Callable<Integer>() {
​
			@Override
			public Integer call() throws Exception {
				
				Integer resultat = 0;
				for (int i = 0; i < 10; i++) {
					resultat += i;
				}
				
				//boolean ret = false;
				//while(!ret); //Danger !!!
				
				return resultat;
			}
			
		});
		*/
		
		Callable<Integer> callback = () -> {				
			Integer resultat = 0;
			for (int i = 0; i < 10; i++) {
				resultat += i;
			}
			Thread.sleep(1000);
			System.out.println("Thread runnable " + Thread.currentThread().getName());
			
			return resultat;
		};
		
		List<Callable<Integer>> ltasks = new ArrayList<Callable<Integer>>();
		for(int j = 0; j < 100; j++) {
			ltasks.add(callback);
		}
		
		List<Future<Integer>> lresultats = (List<Future<Integer>>) es.invokeAll(ltasks);
		
		for(Future<Integer> f : lresultats) {
			
			System.out.println("Future : " + f.get());
		}
		
		/*
		Future<Integer> ret = es.submit(() -> {				
			Integer resultat = 0;
			for (int i = 0; i < 10; i++) {
				resultat += i;
			}
		
			return resultat;
		});
		
		Integer resultat = null;
		try {
			resultat = ret.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Resultat de la Task : " + resultat);
		*/
		es.shutdown();
	}

}









